#设置一个新的 Laravel 项目
- ##创建apidemo项目 (基于laravel5.5)
      composer created-project --prefer-dist laravel/laravel apidemo 5.5.*
- ##服务器要求
    - PHP >= 7.0.0
    - PHP OpenSSL 扩展
    - PHP PDO 扩展
    - PHP Mbstring 扩展
    - PHP Tokenizer 扩展
    - PHP XML 扩展
- ##本地开发服务器
    使用PHP内置的开发环境服务器为应用提供服务       
    
        php artisan serve
        
    在浏览器输入http://localhost:8000 即可访问应用
- ##配置host
        127.0.0.1 apidemo.test    
- ##服务器配置
    服务器配置可能有不同
    
- 将laravel中的public下的index.php作为单一入口
- 给storage和bootstrap/cache目录权限
- 将.env.example文件重命名.env
- 生成（app_key) php artisan key:generate 

        server {
        
            listen 80;
            listen [::]:80;
        
            server_name apidemo.test;
            root /var/www/apidemo/public;
            index index.php index.html index.htm;
        
            location / {
                 try_files $uri $uri/ /index.php$is_args$args;
            }
        
            location ~ \.php$ {
                try_files $uri /index.php =404;
                fastcgi_pass php-upstream;
                fastcgi_index index.php;
                fastcgi_buffers 16 16k;
                fastcgi_buffer_size 32k;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                #fixes timeouts
                fastcgi_read_timeout 600;
                include fastcgi_params;
            }
        
            location ~ /\.ht {
                deny all;
            }
        
            location /.well-known/acme-challenge/ {
                root /var/www/letsencrypt/;
                log_not_found off;
            }
        
            error_log /var/log/nginx/apidemo_error.log;
            access_log /var/log/nginx/apidemo_access.log;
        }

- ##对低版本mysql的支持
    1 将config中的database.php中的字符编码修改为utf8
    2 /app/providers/AppServiceProvider.php 中的boot添加
   
        Schema::defaultStringLength(191);
